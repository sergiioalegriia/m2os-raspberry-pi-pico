Init
    In my case:
        Create the vagrant machine with ubuntu
        Configure it
    At the end of the Init we need a linux machine ready to start working with.


Getting started with Raspberry Pi Pico (https://datasheets.raspberrypi.com/pico/getting-started-with-pico.pdf)

Installing SDK - (Chapter 2)
    1. Download the SDK
        - mkdir pico; cd pico
        - git clone -b master https://github.com/raspberrypi/pico-sdk.git
        - cd pico-sdk
        - git submodule update --init
        - cd ..
        - git clone -b master https://github.com/raspberrypi/pico-examples.git 
    2. Install the tool
        - sudo apt update
        - sudo apt install cmake gcc-arm-none-eabi libnewlib-arm-none-eabi build-essential
        (Ubuntu and debian also intall: libstdc++-arm-none-eabi-newlib)
        - sudo apt install libstdc++-arm-none-eabi-newlib

        - cmake --version
        If version lower than 3.12

    3. Update cmake as the toolchain Downloads version 3.5.1
        - sudo apt remove cmake
        - download the last version from https://cmake.org/download/
        Installing cmake throug command line (https://vitux.com/how-to-install-cmake-on-ubuntu/)
        - sudo apt-get install build-essential libssl-dev
        - sudo apt-get install libssl-dev
        - cd /tmp
        - wget https://github.com/Kitware/CMake/releases/download/v3.22.1/cmake-3.22.1.tar.gz
        - tar -zxvf cmake-3.22.1.tar.gz
        - cd cmake-3.22.1
        - ./bootstrap
        - make
        - sudo make install
        - cmake --version (check if installed)


Building some examples - (Chapter 3)
    pwd = /vagrant/workspace/pico

    Export PICO_SDK_PATH variable 
    export PICO_SDK_PATH=/vagrant/workspace/pico/pico-sdk
    example 1: Blinking a LED
    - cd pico-examples
    - mkdir build
    - cd build
    - cmake ..
    - cd blink
    - make -j4 (So the make is done in parallel)
    

        